<h1>Final Project</h1>
<h3>Kelompok 30</h3>
<br>
<h3>Anggota Kelompok</h3>
<br>
<ul>
    <li>Muhamad Rizky Maulana S</li>
    <li>Mochamad Rizki Ramdhan</li>
</ul>
<br>
<h2>Tema Project</h2>
<p>Aplikasi Penglolaan Inventori Gudang</p>
<h3>ERD</h3>
![gambar](erd_database.png)
<p>Template yang digunakan yaitu pluto versi 1.0.0 yang didapat dari : https://codeload.github.com/technext/pluto/zip/refs/tags/v1.0.0<p>
Penjelasan singkat Project Final : 
Project final yang kami buat yaitu Aplikasi pengelolaan inventori barang dimana aplikasi ini sangat jauh dari kata sempurna. Aplikasi ini masih sangat sangat sederhana dimana aplikasi ini masih perlu untuk dikembangkan lagi.
Aplikasi ini memiliki fitur yang terdiri dari
	1. Penambahan Supplier
	2. Penambahan Kategori barang
	3. Penambahan Barang
	4. Penambahan dan Pengurangan stock barang

Link demo :
https://youtu.be/VZQiMayrHXE

Link deploy : 
http://project-final-inventori.herokuapp.com/
